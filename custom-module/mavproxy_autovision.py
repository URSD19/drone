#!/usr/bin/env python
import time
import math
import os
import os.path
import errno
import struct

from pymavlink import mavutil
from MAVProxy.modules.lib import mp_module
from MAVProxy.modules.lib import mp_util
from MAVProxy.modules.lib import mp_settings
from MAVProxy.modules.lib.mp_settings import MPSetting


import numpy as np

import cv2
import cv2.aruco as aruco

import sys
import threading

import traceback

from collections import deque

class VisionModule(mp_module.MPModule):
    def __init__(self, mpstate):
        super(VisionModule, self).__init__(mpstate, "autovision", public = True)
        
        self.add_command('autovision', self.cmd_autovision, "Autmatic Vision Tracking + land", [
            'on',
            'off',
            'test'])

        
        self.positionData = positionData()
        self.theVision = visualTracker()
        self.theVision.setArucoDictionary()
        
        self.override = [ 0 ] * 16
        
        self.most_recent = None
        self.res_w = 640
        self.res_h = 480

        self.targetFound = None

        #Note: Pitch/Roll trim and PID coefficients need to be adjusted based on
        #the height where the drone will want to land from.
        #For example, a PID of pX = 5, iX = 1.3, dX = .1, pY = 2.2,
        #iY = .6, and dY = .03 is adjusted for a precision landing occuring from 20-25m
        #A landing that occurs lower or higher requires gain coefficients to be
        #lesser or greater respectively.

        #Calibrate for specific motors, 1500 seems to be appropriate value range in simulation
        self.roll_trim = 1502
        self.pitch_trim = 1512

        #PID Gain coefficients: Kp, Ki, Kd (proportional, integral, derivative)

        ############# Roll gain coefficients ###############
        self.Kp_X = 5
        self.Ki_X = 1.3
        self.Kd_X = .1
        ####################################################

        ############# Pitch gain coefficients ###############
        self.Kp_Y = 2.2
        self.Ki_Y = .6
        self.Kd_Y = .03
        #####################################################

        #PID Proportional Errors
        self.Ep_X = 0
        self.Ep_Y = 0
        
        self.Ep_X_old = 0
        self.Ep_Y_old = 0

        #PID Integral Error
        self.Ei_X = deque([0,0,0,0])
        self.Ei_Y = deque([0,0,0,0])    

        #PID Derivative Error
        self.Ed_X = 0
        self.Ed_Y = 0

        #Timer
        #NEED TO ADJUST BASED ON PERFORMANCE
        self.t = 0
        self.dt = .6 #camera update time

        #PID Flags
        self.Ed_flag = True
        self.Ei_flag = True

        self.PWM_roll = 0
        self.PWM_pitch = 0

        self.autovisionLoopFlag = True #Allows user to turn off module if needed

    def cmd_autovision(self, args):
        """ Main algorithm implementation

        """
        usage = "on, test"
        if args[0] == "on": #This is where the actual control loop occurs
            #Flag signals beginning of loops, thread the loops so both can occur simultaneously
            self.autovisionLoopFlag = True
            track_thread = threading.Thread(target = self.imageTracking)
            track_thread.daemon = True
            track_thread.start()

            cam_thread = threading.Thread(target = self.imageUpdate)
            cam_thread.daemon = True
            cam_thread.start()

        if args[0] == "off":
            self.autovisionLoopFlag = False
            ##RPi.GPIO.output(channel_list, low/high) #Set GPIO channels low
            self.status.override = [0, 0, 0, 0, 0, 0, 0, 0]
            
            #PID Variable reset
            self.Ep_X_old = 0
            self.Ep_Y_old = 0
            self.dt = .6
            self.Ei_flag = True
            self.Ed_flag = True

            #PID Proportional Errors
            self.Ep_X = 0
            self.Ep_Y = 0
            
            self.Ep_X_old = 0
            self.Ep_Y_old = 0

            #PID Integral Error
            self.Ei_X = deque([0,0,0,0])
            self.Ei_Y = deque([0,0,0,0])    

            #PID Derivative Error
            self.Ed_X = 0
            self.Ed_Y = 0
            
            print("Autovision module has stopped")
            
        if args[0] == "test":
            print("This is nothing")


    
    
    def imageUpdate(self, png = True): #Change the value of png to false when using a real camera
        #Find the image to be read by our visual tracking program

        path = '..\\..\\..\\..\\..\\..\\Users\\Tommy\\ardupilot\\libraries\\SITL\\examples\\Morse\\camfeed.png' #Strictly for simulation environment
        while(self.autovisionLoopFlag == True):
            if png == False:
                try:
                    _, self.most_recent = cv2.VideoCapture(0)
                    self.theVision.frame = self.most_recent
                    self.theVision.updateCycle()
                    self.theVision.GetMarkerDistance()
                    self.theVision.findAndCompareCentroid()
                    for i in range(4):
                        self.theVision.accumulate()
                except:
                    #traceback.print_exc()
                    pass
                
            else:
                try:
                    self.most_recent = cv2.imread(path, cv2.IMREAD_COLOR)
                    self.theVision.frame = self.most_recent
                    self.theVision.updateCycle()
                    self.theVision.GetMarkerDistance()
                    self.theVision.findAndCompareCentroid()
                    for i in range(4):
                        self.theVision.accumulate()
                except:
                    #traceback.print_exc()
                    pass

    def imageTracking(self):
        #Run visual tracking algorithm
        
        while(self.autovisionLoopFlag == True):
            if self.status.flightmode == 'LAND': #Activate when drone is put on "LAND" mode.
                self.trackPad()

    def send_rc_override(self): #Motor override, borrowed from mavproxy_rc module
        '''send RC override packet'''
        if self.sitl_output:
            buf = struct.pack('<HHHHHHHHHHHHHHHH',
                              *self.override)
            self.sitl_output.write(buf)
        else:
            chan8 = self.override[:8]
            self.master.mav.rc_channels_override_send(self.target_system,
                                                           self.target_component,
                                                           *chan8)

        
    def trackPad(self):
        #PID Control and visual tracking implementation
        try:
            if self.status.flightmode == 'LAND': # and self.theVision.historyData['pos_data'][0] != None:
                self.t = time.time()

                #if abs(self.theVision.historyData['pos_data'][0]) < .2 and abs(self.theVision.historyData['pos_data'][1]) < .2:
                #    self.status.flightmode = 'LAND'

                #if self.theVision.historyData['target_found'] == False:
                #    self.status.flightmode = 'LAND'
                                    
                #Proportional error, PID control
                print("X CENTER POINT =========== ", self.theVision.centerPoints[0])

                self.Ep_X = (self.theVision.centerPoints[0] - 320) / 320 #X prop. error,  X position of center - half of resolution width / half of resolution width
                self.Ep_Y = (self.theVision.centerPoints[1] - 240) / 240

                #Integral error, PID control
                self.Ei_X.pop()
                self.Ei_X.appendleft(self.Ep_X*self.dt)

                self.Ei_Y.pop()
                self.Ei_Y.appendleft(self.Ep_Y*self.dt)    
                    
                if self.Ei_flag == True:
                    self.Ei_X = deque([0,0,0,0])
                    self.Ei_Y = deque([0,0,0,0])
                    self.Ei_flag = False

                #Derivative error, PID control
                if self.dt != 0:
                    self.Ed_X = (self.Ep_X - self.Ep_X_old) / self.dt
                    self.Ed_Y = (self.Ep_Y - self.Ep_Y_old) / self.dt
                if self.Ed_flag == True:
                    self.Ed_X = 0
                    self.Ed_Y = 0
                    self.Ed_flag = False

                #PID PWM control signal calculations for roll & pitch
                self.PWM_roll = (self.Kp_X * self.Ep_X) + (self.Ki_X * sum(self.Ei_X)) + (self.Kd_X * self.Ed_X)
                self.PWM_pitch = (self.Kp_Y * self.Ep_Y) + (self.Ki_Y * sum(self.Ei_Y)) + (self.Kd_Y * self.Ed_Y)

                self.PWM_roll = self.roll_trim + int(self.PWM_roll*50)
                self.PWM_pitch = self.pitch_trim - int(self.PWM_pitch*50)

                #print("X (meters) = ", self.theVision.historyData['pos_data'][0])
                #print("Y = (meters) = ", self.theVision.historyData['pos_data'][1])
                print("X error: P = ", self.Ep_X, " I = ", self.Ei_X, " D = ", self.Ed_X, "\n")
                print("Y error: P = ", self.Ep_Y, " I = ", self.Ei_Y, " D = ", self.Ed_Y, "\n")
                print("PWM_pitch = ", self.PWM_pitch)
                print("PWM_roll = ", self.PWM_roll)
                print("dt = ", self.dt)

                #Override is a list that accepts 8 values, first 4 values correspond to
                #roll, pitch, throttle, and yaw respectively. Not sure what the other 4 channels do.
                #Channel correspondence is based on information on ardupilot's online docs.


                #RPi.GPIO.output(channel_list, low/high) ??? #May want to activate GPIO when testing on real drone
                self.override[0] = self.PWM_roll #Send initial roll and pitch signals calculated based on PID controller
                self.override[1] = self.PWM_pitch
                self.send_rc_override()
                time.sleep(0.3)

                self.override[0] = self.roll_trim
                self.override[1] = self.pitch_trim
                self.send_rc_override()
                time.sleep(0.3)
                
            else:
                ##RPi.GPIO.output(channel_list, low/high) #Set GPIO channels low
                self.status.override = [0, 0, 0, 0, 0, 0, 0, 0]
                self.send_rc_override()
                #PID Variable reset
                self.Ei_X = deque([0, 0, 0, 0])
                self.Ei_Y = deque([0, 0, 0, 0])
                self.Ep_X_old = 0
                self.Ep_Y_old = 0
                self.dt = .6
                self.Ei_flag = True
                self.Ed_flag = True

            self.Ep_X_old = self.Ep_X
            self.Ep_Y_old = self.Ep_Y
            self.dt = time.time() - self.t
            #cv2.imshow("feed", self.theVision.frame)
        except:
            #print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
            traceback.print_exc()


        

        
class positionData():
    def __init__(self):
        self.directionalData = {'X': None, 'Y': None, 'Z': None}
        self.altitude = None
        self.angularDirection = {"Left": None, "Right": None,
                                "Fwd": None, "Bkwd": None}
        self.rotationalData = None
        self.targetFound = False
        self.onTarget = False
        self.centerPoints = []
        self.recFound = True
        self.log = True

        #self.posBuffer = np.array()
        self.Xbuf = []
        self.Ybuf = []
        self.Zbuf = []
        #self.rotBuffer = np.array()
        self.targetBuffer = []
        self.X = None

        self.historyData = {'pos_data': None, 'rotataion_data': None, 'target_found': None}

    def accumulate(self, frames): #Average information across specified number of camera frames
        #Average positional XYZ Data
        if(self.log == True):
            self.Xbuf.append(self.directionalData['X']) #Append data to buffer arrays
            self.Ybuf.append(self.directionalData['Y'])
            self.Zbuf.append(self.directionalData['Z'])
            #self.rotBuffer.append(self.rotationalData)
            self.targetBuffer.append(self.targetFound)
                                                        
        
        if(len(self.Xbuf) == frames):
            try:
                #filter(self.Xbuf, None) #Filter null values from X, Y, Z buffers
                #filter(self.Ybuf, None)
                #filter(self.Zbuf, None)
                self.Xbuf = [x for x in self.Xbuf if x is not None]
                self.Ybuf = [x for x in self.Ybuf if x is not None]
                self.Zbuf = [x for x in self.Zbuf if x is not None]
                self.X = sum(self.Xbuf)/len(self.Xbuf) #Take average value
                self.Y = sum(self.Ybuf)/len(self.Ybuf)
                self.Z = sum(self.Zbuf)/len(self.Zbuf)

                self.historyData['pos_data'] = [self.X, self.Y, self.Z] #Store in history data dictionary
            except:
                self.historyData['pos_data'] = None
                
            self.Xbuf = [] #Clear buffers
            self.Ybuf = []
            self.Zbuf = []

            booleanCheck = sum(self.targetBuffer) #Sum boolean list to obtain number of 'True' statements

            if booleanCheck >= (int(frames/2)): #If 'True' is in majority, target is in the view of the camera
                self.historyData['target_found'] = True
            else:
                self.historyData['target_found'] = False


class visualTracker():
    def __init__(self):
        self.contours = None
        self.vid = None
        self.HSVthreshold = []

        self.approxPoly = None
        self.transformedImg = None

        self.objImgHeight = None
        self.objImgWidth = None

        self.directionalData = {'X': None, 'Y': None, 'Z': None}
        self.altitude = None
        self.angularDirection = {"Left": None, "Right": None,
                                "Fwd": None, "Bkwd": None}
        self.rotationalData = None
        self.targetFound = False
        self.onTarget = False
        self.centerPoints = []
        self.recFound = True
        self.log = True

        #self.posBuffer = np.array()
        self.Xbuf = []
        self.Ybuf = []
        self.Zbuf = []
        #self.rotBuffer = np.array()
        self.targetBuffer = []
        self.X = None

        self.historyData = {'pos_data': None, 'rotataion_data': None, 'target_found': None}

    def accumulate(self): #Average information across specified number of camera frames
        #Average positional XYZ Data
        if(self.log == True):
            self.Xbuf.append(self.directionalData['X']) #Append data to buffer arrays
            self.Ybuf.append(self.directionalData['Y'])
            self.Zbuf.append(self.directionalData['Z'])
            #self.rotBuffer.append(self.rotationalData)
            self.targetBuffer.append(self.targetFound)
            frames = 4
                                                        
        
        if(len(self.Xbuf) == frames):
            try:
                #filter(self.Xbuf, None) #Filter null values from X, Y, Z buffers
                #filter(self.Ybuf, None)
                #filter(self.Zbuf, None)
                self.Xbuf = [x for x in self.Xbuf if x is not None]
                self.Ybuf = [x for x in self.Ybuf if x is not None]
                self.Zbuf = [x for x in self.Zbuf if x is not None]
                self.X = sum(self.Xbuf)/len(self.Xbuf) #Take average value
                self.Y = sum(self.Ybuf)/len(self.Ybuf)
                self.Z = sum(self.Zbuf)/len(self.Zbuf)

                self.historyData['pos_data'] = [self.X, self.Y, self.Z] #Store in history data dictionary
            except:
                self.historyData['pos_data'] = None
                
            self.Xbuf = [] #Clear buffers
            self.Ybuf = []
            self.Zbuf = []

            booleanCheck = sum(self.targetBuffer) #Sum boolean list to obtain number of 'True' statements

            if booleanCheck >= (int(frames)/2): #If 'True' is in majority
                self.historyData['target_found'] = True
            else:
                self.historyData['target_found'] = False

            #print("X = ", self.historyData['pos_data'][0], "m,  Y = ", self.historyData['pos_data'][1], "m,  Z = ", self.historyData['pos_data'][2], "m" )

    def findAndCompareCentroid(self, Test=False):
        #Find the central point of the quadrilateral 

        w = 640 #self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        l = 480 #self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
        for contour in self.corners:
            moments = cv2.moments(contour) #Get the moments of the contour
            #Center computation through contour moments:
            centX = int(moments["m10"] / moments["m00"])
            centY = int(moments["m01"] / moments["m00"])
            self.centerPoints = [centX, centY]
            cv2.rectangle(self.frame, (300, 260), (340, 220), (0, 0, 255), 1)
            cv2.line(self.frame, ( int(w/2), 0 ), ( int(w/2), int((l/2)-20)), (0, 0, 255), 1)
            cv2.line(self.frame, ( 0, int(l/2) ), ( int((w/2)-20), int(l/2)), (0, 0, 255), 1)
            cv2.line(self.frame, ( int(w/2), int((l/2)+20) ), ( int(w/2), int(l) ), (0, 0, 255), 1)
            cv2.line(self.frame, ( int((w/2)+20), int(l/2) ), ( int(w), int(l/2)), (0, 0, 255), 1)
            if 300 <= centX <= 340 and 220 <= centY <= 260:
                cv2.rectangle(self.frame, (300, 260), (340, 220), (0, 255, 0), 1)
                cv2.line(self.frame, ( int(w/2), 0 ), ( int(w/2), int((l/2)-20)), (0, 255, 0), 1)
                cv2.line(self.frame, ( 0, int(l/2) ), ( int((w/2)-20), int(l/2)), (0, 255, 0), 1)
                cv2.line(self.frame, ( int(w/2), int((l/2)+20) ), ( int(w/2), int(l) ), (0, 255, 0), 1)
                cv2.line(self.frame, ( int((w/2)+20), int(l/2) ), ( int(w), int(l/2)), (0, 255, 0), 1)
                self.onTarget = True

            if Test == True:
                cv2.circle(self.frame, (centX, centY), 4, (0, 0, 255), 0)

    def GetMarkerDistance(self):
        #Pose estimation of ArUco markers, output of translation matrix is in meters
        self.corners, self.ids, _ = aruco.detectMarkers(self.frame, self.aruco_dict, parameters = self.parameters)
        imgPnts = self.corners
        if(self.ids == None):
            self.targetFound = False
        else:
            self.targetFound = True

        #f = 20, sensorW = 32, sensorL = 32, and objPnts are adjust for simulation, change for real drone
        f = 20 #mm, focal length as provided in camera data sheet
        
        resX = 640 #Camera resolution variables
        resY = 480
        
        sensorW = 32 #3.60 #mm, width of actual sensor
        sensorL = 32 #2.00 #mm, height of actual sensor
        fx = f*resX/sensorW
        fy = f*resY/sensorL
        cenX = resX/2 #self.centerPoints[0]
        cenY = resY/2 #self.centerPoints[1]
        objPnts = np.array([[0.0, 0.0, 0.0], [0.0, .0174, -0.0174, 0.0], [0.0174, 0.0, 0.0], [0.0, -0.0174 ,0.0]]) #numpy array of four points in order of (topL, botR, topR, botL), 3-D coordinates
        camMat = np.array([[fx, 0, cenX], [0, fy, cenY], [0, 0, 1]], dtype = "double")
        disCoeffs = np.array([0, 0, 0, 0])
        if np.all(self.ids != None):
            #Runs pose estimation of ArUco marker based on detected corners in grayscale image, marker side size, camera matrix, and distortion coefficients
            rvec, tvec, _ = aruco.estimatePoseSingleMarkers(self.corners, .77, camMat, disCoeffs)
            xyzMat = np.matrix([tvec[0][0][0], tvec[0][0][1], tvec[0][0][2]])
            xyzMat = xyzMat.reshape(3, 1)
            rotationMat = cv2.Rodrigues(rvec)[0]
            camPos = -np.matrix(rotationMat).T * xyzMat
            #print("\n", rvec[0], "\n")
            #print(camPos)
            X = float(camPos[0])
            Y = float(camPos[1])
            Z = float(camPos[2])
            self.directionalData['X'] = round(X, 4)
            self.directionalData['Y'] = round(Y, 4)
            self.directionalData['Z'] = round(Z, 4)
            #print("X = ", self.directionalData['X'], "m: Y = ", -self.directionalData['Y'], "m: Z = ", self.directionalData['Z'])
            #aruco.drawAxis(self.frame, camMat, disCoeffs, rvec, tvec, .6)    

            ##   __ __
            ##| fx 0 self.centerPoints[0] |
            ##| 0 fy self.centerPoints[1] |
            ##|_0 0             1       _

    def setCamera(self, camera): #(unused for now)
        #Set the desired camera 

        self.vid = cv2.VideoCapture(camera)

    def updateCycle(self):
        self.contours = None
        self.approxPoly = None
        self.targetFound = False

        self.objImgHeight = None
        self.objImgWidth = None

        self.directionalData = {'X': None, 'Y': None, 'Z': None}

    def setArucoDictionary(self):
        #Set the reference image for the target that will be identified

        self.aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_1000) #Retrieve dictionary of aruco markers
        self.parameters = aruco.DetectorParameters_create()
        
    
def init(mpstate):
    """ Initialise module

    """
    return VisionModule(mpstate)
