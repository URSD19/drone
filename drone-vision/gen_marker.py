import numpy as np
import cv2
import cv2.aruco as aruco
from matplotlib import pyplot as plt, cm
import sys
i = int(sys.argv[1])

aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
img = aruco.drawMarker(aruco_dict,i, 500)

fig = plt.figure()
plt.axis("off")
plt.imshow(img, cmap = cm.gray, interpolation = "nearest")
plt.savefig("_img/m%d.png" % i)
