import numpy as np
import cv2

import time
import os
from positionalData import positionData
import visualTrack
import traceback

positionData = positionData()
theVision = visualTrack.visualTracker()
theVision.setPositionData(positionData)

#theVision.setCamera(0)
#w = theVision.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
#l = theVision.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
#print(l,w)
theVision.setPositionData(positionData)
theVision.setHSVThreshold([100, 150, 0, 180, 255, 255]) #HSV Threshold values for blue
theVision.setArucoDictionary()

while(True):
    theVision.updateCycle(png = True, path = "camfeed.png")
    #theVision.findContour()
    #theVision.approxQuad(Test = True)
    try:
        theVision.GetMarkerDistance()
        theVision.findAndCompareCentroid(Test = True)
        if positionData.onTarget == True:
            print("X =", positionData.directionalData['X'], "m:  Y =", positionData.directionalData['Y'], "m:  Z =", positionData.directionalData['Z'], ";  The object is on target." )
        else:
            print("X =", positionData.directionalData['X'], "m:  Y =", positionData.directionalData['Y'], "m:  Z =", positionData.directionalData['Z'], ";  The object is not on target." )

        #positionData.accumulate(10) #Accumulate data from 10 frames
            
    except:
        pass
    cv2.imshow("Vision Window", theVision.frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break 

