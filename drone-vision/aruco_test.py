import numpy as np
import cv2
import cv2.aruco as aruco


cap = cv2.VideoCapture(0)
marker_size = 1 #ft


f = 4.1 #mm, focal length as provided in camera data sheet

resX = 640 #Camera resolution variables
resY = 480

sensorW = 3.60 #mm, width of actual sensor
sensorL = 2.00 #mm, height of actual sensor
fx = f*resX/sensorW
fy = f*resY/sensorL
cenX = resX/2#self.posData.centerPoints[0]
cenY = resY/2#self.posData.centerPoints[1]
objPnts = np.array([[0.0, 0.0, 0.0], [0.112, -0.144, 0.0], [0.112, 0.0, 0.0], [0.0, -0.144 ,0.0]]) #numpy array of four points in order of (topL, botR, topR, botL), 3-D coordinates
matrix = np.array([[fx, 0, cenX], [0, fy, cenY], [0, 0, 1]], dtype = "double")
distortion = np.array([0, 0, 0, 0]) 

from numpy import cross, eye, dot
from scipy.linalg import expm

def M(rvec):
    return expm(cross(eye(3), rvec))

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    #print(frame.shape) #480x640
    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_1000)
    parameters =  aruco.DetectorParameters_create()

    #print(parameters)

    '''    detectMarkers(...)
        detectMarkers(image, dictionary[, corners[, ids[, parameters[, rejectedI
        mgPoints]]]]) -> corners, ids, rejectedImgPoints
        '''
        #lists of ids and the corners beloning to each id
    corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    #print(rejectedImgPoints)
    # Display the resulting frame
    cv2.imshow('frame',gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    
    
    if len(corners) == 0 :
        continue
    elif len(corners) > 1 : 
        print("too many: %d" % len(corners))


    #It's working.
    # my problem was that the cellphone put black all around it. The alrogithm
    # depends very much upon finding rectangular black blobs

    rvecs, tvecs, _ = aruco.estimatePoseSingleMarkers(corners, marker_size, matrix, distortion)
    rvec, tvec = rvecs[0], tvecs[0]
   
    #get drone location in pad coordinates 
    matrix = M(-rvec)
    pad2drone = np.matmul( matrix , np.transpose(-tvec) )
    print(pad2drone)
        

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
