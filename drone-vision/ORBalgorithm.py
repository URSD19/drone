import cv2
import math
import numpy as np

from pntTransform import *
from ContourDetection import *

BLUE = [90,200,210,130,255,255] #Threshold array for the color blue
YELLOW = [10,100,90,30,255,255] #Threshold array for the color yellow

vid = cv2.VideoCapture(1)

detector = cv2.ORB_create()

r_threshold = .835

img = cv2.imread("target3.png")

matcher = cv2.BFMatcher(cv2.NORM_HAMMING)

while(True):
    ret, frame = vid.read()
    hsv_vid = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
            
    contours = findContour(BLUE, hsv_vid)
    for x in range(len(contours)):
        cont_area = cv2.contourArea(contours[x])
        if cont_area > 300:
            arclength = cv2.arcLength(contours[x], True) #Retrieve contour perimeter, second argument denotes that it is a closed contour
            approx_poly = cv2.approxPolyDP(contours[x], (0.08*arclength), True) #Approximate contour shape
            if len(approx_poly) == 4: #If contour has exactly 4 corners/is a parallelogram
                cv2.drawContours(frame, [approx_poly], 0, (0, 0, 255), 2) #Draw the contrours on the frame
                warp = rectTransform(frame, approx_poly)
                vid_gray = cv2.cvtColor(warp, cv2.COLOR_BGR2GRAY)
                img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

                (kps_vid, descs_vid) = detector.detectAndCompute(vid_gray, None)
                (kps_img, descs_img) = detector.detectAndCompute(img_gray, None)
                # Need to draw only good matches
                matches = matcher.knnMatch(descs_vid, descs_img, k = 2)
                good = []
                # ratio test
                for m, n in matches:
                    if m.distance < 0.85*n.distance:
                        good.append([m])
                im3 = cv2.drawMatchesKnn(warp, kps_vid, img, kps_img, good[1:20], None, flags=2)
                cv2.imshow("Test1", im3)
        else:
            break
    cv2.imshow("Test2", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
