import numpy as np
import cv2
import cv2.aruco as aruco

import traceback
import sys
import time

import math

class visualTracker():
    def __init__(self):
        self.contours = None
        self.vid = None
        self.HSVthreshold = []
        
        self.detector = cv2.AKAZE_create()
        self.matcher = cv2.BFMatcher(cv2.NORM_HAMMING)

        self.approxPoly = None
        self.transformedImg = None

        self.objImgHeight = None
        self.objImgWidth = None

    def pntRestructure(self, points):
        """ Restructure points in contour

        """
        cont_shape = points.reshape((4, 2))
        restructured_pts = np.zeros((4, 2), dtype = np.float32)

        s = cont_shape.sum(1)
        restructured_pts[0] = cont_shape[np.argmin(s)]
        restructured_pts[2] = cont_shape[np.argmax(s)] 

        d = np.diff(cont_shape, axis = 1)
        restructured_pts[1] = cont_shape[np.argmin(d)]
        restructured_pts[3] = cont_shape[np.argmax(d)]
    
        self.approxPoly = restructured_pts #Return in the form of (top-left, bottom-right, top-right, bottom-left)

    def rectTransform(self):
        rect = self.approxPoly
        live_img = self.frame
        (topL, botR, topR, botL) = rect #Retrieve the ordered points from pt_ordering()

        #Width == max distance b/t bottom left/right x coordinates and top left/right x coordinates

        widthT = np.sqrt(( (botR[0] + botL[0])**2) + ((botR[1] + botL[1])**2) )
        widthB = np.sqrt(( (topR[0] + topL[0])**2) + ((topR[1] + topL[1])**2) )
        maxW = max(int(widthT), int(widthB))

        #Width == max distance b/t right top/bottom y coordinates and left top/bottom y coordinates
        heightR = np.sqrt(( (topR[0] + botR[0])**2) + ((topR[1] + botR[1])**2) )
        heightL = np.sqrt(( (topL[0] + botL[0])**2) + ((topL[1] + botL[1])**2) )
        maxH = max(int(heightR), int(heightL))

        dest = np.array([
                    [0, 0],
                    [maxW - 1, 0],
                    [maxW - 1, maxH - 1],
                    [0, maxH - 1]], dtype = "float32") #Top left, top right, bottom right, bottom left

        transMat = cv2.getPerspectiveTransform(rect, dest) #Transformation matrix
        warped_img = cv2.warpPerspective(live_img, transMat, (maxW, maxH))

        self.transformedImg = warped_img

    def findContour(self):
        thresArray = self.HSVThreshold
        hsvImg = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)
        lower_thres = np.array([thresArray[0], thresArray[1], thresArray[2]]) #Lower threshold for blue
        upper_thres = np.array([thresArray[3], thresArray[4], thresArray[5]]) #Upper threshold

        img_thres = cv2.inRange(hsvImg, lower_thres, upper_thres) #Define threshold for HSV version of video feed

        self.contours, _= cv2.findContours(img_thres, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) #Retrieve contour data
        cv2.imshow("HSV Img", hsvImg)

    def approxQuad(self, Test = False):
        """ Try to approximate a quadrilateral shape given a contour and return the coordinates of its edges

        """
        try:
            for x in range(len(self.contours)):
                area = cv2.contourArea(self.contours[x]) #Find area of  contour
                if area > 500: #Discard especially small quadrilaterals
                    arclength = cv2.arcLength(self.contours[x], True) #Retrieve contour perimeter, second argument denotes that it is a closed contour
                    approx_poly = cv2.approxPolyDP(self.contours[x], (0.08*arclength), True) #Approximate contour shape
                    if len(approx_poly) == 4:
                        self.pntRestructure(approx_poly) #Restructure the points of the approximated polygons and
                        if Test == True:
                            self.approx_contour = np.array([approx_poly], dtype=np.int32)
                            self.unstrucPoly = cv2.drawContours(self.frame, [approx_poly], 0, (255, 100, 255), 2)
        except:
            #traceback.print_exc()
            pass

    def findAndCompareCentroid(self, Test=False):
        """ Find the central point of the quadrilateral 

        """
        w = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        l = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
        for contour in self.corners:
            moments = cv2.moments(contour) #Get the moments of the contour
            #Center computation through contour moments:
            centX = int(moments["m10"] / moments["m00"])
            centY = int(moments["m01"] / moments["m00"])
            self.posData.centerPoints = [centX, centY]
            cv2.rectangle(self.frame, (300, 260), (340, 220), (0, 0, 255), 1)
            cv2.line(self.frame, ( int(w/2), 0 ), ( int(w/2), int((l/2)-20)), (0, 0, 255), 1)
            cv2.line(self.frame, ( 0, int(l/2) ), ( int((w/2)-20), int(l/2)), (0, 0, 255), 1)
            cv2.line(self.frame, ( int(w/2), int((l/2)+20) ), ( int(w/2), int(l) ), (0, 0, 255), 1)
            cv2.line(self.frame, ( int((w/2)+20), int(l/2) ), ( int(w), int(l/2)), (0, 0, 255), 1)
            if 300 <= centX <= 340 and 220 <= centY <= 260:
                cv2.rectangle(self.frame, (300, 260), (340, 220), (0, 255, 0), 1)
                cv2.line(self.frame, ( int(w/2), 0 ), ( int(w/2), int((l/2)-20)), (0, 255, 0), 1)
                cv2.line(self.frame, ( 0, int(l/2) ), ( int((w/2)-20), int(l/2)), (0, 255, 0), 1)
                cv2.line(self.frame, ( int(w/2), int((l/2)+20) ), ( int(w/2), int(l) ), (0, 255, 0), 1)
                cv2.line(self.frame, ( int((w/2)+20), int(l/2) ), ( int(w), int(l/2)), (0, 255, 0), 1)
                self.posData.onTarget = True

            if Test == True:
                cv2.circle(self.frame, (centX, centY), 4, (0, 0, 255), 0)

    def computePixLW(self):
        """ Compute the length and width in pixels for the object being viewed (quadrilateral approximation)

        """
        self.objImgHeight = math.sqrt((self.approxPoly[0][0] - self.approxPoly[3][0])**2 + (self.approxPoly[0][1] - self.approxPoly[3][1])**2)
        self.objImgWidth = math.sqrt((self.approxPoly[0][0] - self.approxPoly[2][0])**2 + (self.approxPoly[0][1] - self.approxPoly[2][1])**2)

        
    def calculateAltitude(self):
        """ Compute the distance in mm, convert to m.

        """
        self.computePixLW()
        imgHeight = 480
        focalLength = 4 #mm
        realHeight = 84
        sensorHeight = 2
        self.posData.altitude = (focalLength*realHeight*imgHeight)/(self.objImgHeight*sensorHeight) #Calculate distance using above data
        #print("Distance = ", round((self.posData.altitude/1000), 5), "m")

    def XYDistanceToCam(self, altitude):
        """ Compute XY distance (potentially outdated)

        """
        camFoV = 60
        physW = (2.02/1000) #Physical width of sensor == 2.02mm (adjusted for meters)
        physL = (3.58/1000) #Physical width of sensor == 2.02mm (adjusted for meters)
        THx = self.posData.centerPoints[0]*camFoV/physW
        THy = self.posData.centerPoints[1]*camFoV/physL
        self.posData.directionalData['X'] = (altitude/1000)*math.tan(THx)
        self.posData.directionalData['Y'] = (altitude/1000)*math.tan(THy)
        #(self.posData.directionalData['X'], self.posData.directionalData['Y'])

    def GetMarkerDistance(self):
        """ Pose estimation of ArUco markers, output of translation matrix is in m 

        """
        gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        self.corners, self.ids, _ = aruco.detectMarkers(gray , self.aruco_dict, parameters = self.parameters)
        imgPnts = self.corners
        if(self.ids == None):
            self.posData.targetFound = False
        else:
            self.posData.targetFound = True
            
        f = 20 #mm, focal length as provided in camera data sheet
        
        resX = 640 #Camera resolution variables
        resY = 480
        
        sensorW = 32 #3.60 #mm, width of actual sensor
        sensorL = 32 #2.00 #mm, height of actual sensor
        fx = f*resX/sensorW
        fy = f*resY/sensorL
        cenX = resX/2 #self.posData.centerPoints[0]
        cenY = resY/2 #self.posData.centerPoints[1]
        objPnts = np.array([[0.0, 0.0, 0.0], [0.0, .0174, -0.0174, 0.0], [0.0174, 0.0, 0.0], [0.0, -0.0174 ,0.0]]) #numpy array of four points in order of (topL, botR, topR, botL), 3-D coordinates
        camMat = np.array([[fx, 0, cenX], [0, fy, cenY], [0, 0, 1]], dtype = "double")
        disCoeffs = np.array([0, 0, 0, 0])
        if np.all(self.ids != None):
            #Runs pose estimation of ArUco marker based on detected corners in grayscale image, marker side size, camera matrix, and distortion coefficients
            rvec, tvec, _ = aruco.estimatePoseSingleMarkers(self.corners, .77, camMat, disCoeffs)
            #rotM = cv2.Rodrigues(rvec)[0]
            #camPosition = -np.matrix(rotM).T * np.matrix(tvec)
            xyzMat = np.matrix([tvec[0][0][0], tvec[0][0][1], tvec[0][0][2]])
            xyzMat = xyzMat.reshape(3, 1)
            #print(xyzMat)
            rotationMat = cv2.Rodrigues(rvec)[0]
            camPos = -np.matrix(rotationMat).T * xyzMat
            #print("\n", rvec[0], "\n")
            #print(camPos)
            X = float(camPos[0])
            Y = float(camPos[1])
            Z = float(camPos[2])
            self.posData.directionalData['X'] = round(X, 4)
            self.posData.directionalData['Y'] = round(Y, 4)
            self.posData.directionalData['Z'] = round(Z, 4)
            #print("X = ", self.posData.directionalData['X'], "m: Y = ", -self.posData.directionalData['Y'], "m: Z = ", self.posData.directionalData['Z'])
            #aruco.drawAxis(self.frame, camMat, disCoeffs, rvec, tvec, .6)    
        ##   __ __
        ##| fx 0 self.posData.centerPoints[0] |
        ##| 0 fy self.posData.centerPoints[1] |
        ##|_0 0             1               _
        
    def setHSVThreshold(self, array):
        """ Obtain an HSV threshold for the colors which should be found
            
        """
        self.HSVThreshold = array
        
    def setCamera(self, camera):
        """ Set the desired camera

        """
        self.vid = cv2.VideoCapture(camera)

    def updateCycle(self, png=False, path = None):
        if png == False:
            ret, self.frame = self.vid.read()
        else:
            self.frame = path
        self.contours = None
        self.approxPoly = None
        self.posData.targetFound = False
        self.posData.centerPoints = []

        self.objImgHeight = None
        self.objImgWidth = None

        self.posData.directionalData = {'X': None, 'Y': None, 'Z': None}

    def setArucoDictionary(self):
        """ Set the reference image for the target that will be identified

        """
        self.aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_1000) #Retrieve dictionary of aruco markers
        self.parameters = aruco.DetectorParameters_create()
        
    
    def setMain(self, main):
        """ Tie visualTrack object to main loop to communicate data

        """
        self.main = main

    def setPositionData(self, posData):
        """ Allow object to write data to positional data variables for access in the main loop

        """
        self.posData = posData
