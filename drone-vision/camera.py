#This is test code for apriltag detection

import apriltag
import numpy as np
import cv2 traceback sys math
import jsonpickle as pickle
import jsonpickle.ext.numpy as jsonpickle_numpy
jsonpickle_numpy.register_handlers()

class Camera():
    SCANNING = 0
    PRIMARY = 1
    TESTING = 2

    def __init__(self, camera):
        self.camera = camera
        self.video = cv2.VideoCapture(camera)

        self.id = "c"+str(camera) #todo
        self.load()
        
    def load(self):
        try: 
            f = open("cam/%s.config" % self.id)
        except IOError:
            self.name = "unnamed"
            self.type = [Camera.SCANNING]
            self.mount = np.Vector([0,0,-1]) #z is up
            self.params = 
            self.save()
        else:
            with f:
                obj = pickle.decode(f.read())        
                self.name = obj.name
                self.type = obj.type
                self.mount = obj.mount
                self.params = obj.params
        
    def save(self): 
        class obj:
            name = self.name
            type = self.type
            mount = self.mount 
            params = self.params

        f = open("cam/%s.config" % self.id, "w")
        f.write(pickle.encode(obj))        
        
    #static methods
    _cameras = {}
    def list():
        return list(Camera._cameras.values())
        
    def get(cid=None):
        if cid=None:
            #there is no way to get the list of opencv cameras.
            #so we scan 100 indices
            for cid in range(100):
                Camera.get(cid)
            return self.camera.list()
        
        if cid in Camera._cameras:
            return Camera._cameras.cid
        
        camera = Camera(cid)
        Camera.map[cid] = camera
        return camera
            

class Vision():
    def __init__(self):
        self.detector = apriltag.Detector()


