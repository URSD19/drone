import numpy as np
import cv2

import traceback

class positionData(object):
    def __init__(self):
        self.directionalData = {'X': None, 'Y': None, 'Z': None}
        self.altitude = None
        self.angularDirection = {"Left": None, "Right": None,
                                "Fwd": None, "Bkwd": None}
        self.rotationalData = None
        self.targetFound = False
        self.onTarget = False
        self.centerPoints = []
        self.recFound = True
        self.log = True

        #self.posBuffer = np.array()
        self.Xbuf = []
        self.Ybuf = []
        self.Zbuf = []
        #self.rotBuffer = np.array()
        self.targetBuffer = []
        self.X = None

        self.historyData = {'pos_data': None, 'rotataion_data': None, 'target_found': None}
        
        

    def accumulate(self, frames): #Average information across specified number of camera frames
        #Average positional XYZ Data
        if(self.log == True):
            self.Xbuf.append(self.directionalData['X']) #Append data to buffer arrays
            self.Ybuf.append(self.directionalData['Y'])
            self.Zbuf.append(self.directionalData['Z'])
            #self.rotBuffer.append(self.rotationalData)
            self.targetBuffer.append(self.targetFound)
                                                        
        
        if(len(self.Xbuf) == frames):
            try:
                """
                filter(self.Xbuf, None) #Filter null values from X, Y, Z buffers
                filter(self.Ybuf, None)
                filter(self.Zbuf, None)
                """
                self.Xbuf = [x for x in self.Xbuf if x is not None]
                self.Ybuf = [x for x in self.Ybuf if x is not None]
                self.Zbuf = [x for x in self.Zbuf if x is not None]
                self.X = sum(self.Xbuf)/len(self.Xbuf) #Take average value
                self.Y = sum(self.Ybuf)/len(self.Ybuf)
                self.Z = sum(self.Zbuf)/len(self.Zbuf)

                self.historyData['pos_data'] = [self.X, self.Y, self.Z] #Store in history data dictionary
            except:
                self.historyData['pos_data'] = None
                
            self.Xbuf = [] #Clear buffers
            self.Ybuf = []
            self.Zbuf = []

            booleanCheck = sum(self.targetBuffer) #Sum boolean list to obtain number of 'True' statements

            if booleanCheck >= (frames/2): #If 'True' is in majority
                self.historyData['target_found'] = True
            else:
                self.historyData['target_found'] = False

            print("X = ", self.historyData['pos_data'][0], "m,  Y = ", self.historyData['pos_data'][1], "m,  Z = ", self.historyData['pos_data'][2], "m" )
            
            
            

            
