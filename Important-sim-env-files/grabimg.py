import pymorse
#import numpy
#import scipy.misc
from PIL import Image
import base64

import cv2
import cv2.aruco as aruco

import sys
import time

import math

import traceback

sim = pymorse.Morse()

##positionData = positionData()
##theVision = visualTrack.visualTracker()
##theVision.setPositionData(positionData)
##
###theVision.setCamera(0)
###w = theVision.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
###l = theVision.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
###print(l,w)
##theVision.setPositionData(positionData)
##theVision.setHSVThreshold([100, 150, 0, 180, 255, 255]) #HSV Threshold values for blue
##theVision.setArucoDictionary()

while(True):
    data = sim.drone.Camera.stream.get()

    buff = base64.b64decode( data['image'] )

    ### Using PIL

    image = Image.frombuffer('RGBA', (640, 480), buff, 'raw', 'RGBA', 0, 1)
    image.save("camfeed.png")
    time.sleep(1)
