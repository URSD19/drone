# Drone planning
Notes:
	Copter design:
		Configuration:
			4, 6, or 8 motors, 
coaxial motors? Probably not.
Rotating arms? Cool but not practical for our purposes.

	Copter software:
		Detect pad visually
		Precise motion and position tracking.
GPS flight
Talk to server with cellular?
		Have light for night landing?
		
		STRETCH GOALS:
		Avoid obstacles
		Avoid collisions with other drones.
	Pad design.
		Functions:
			Connect to wifi (perhaps cell as well?)
			Talk to server program over network
			Talk to drone? (somehow)
				Alternatively the drone could have cell.
			Have a visual identifier.
				Possibly a light up identifier for night time?
			Have a homing beacon?
				GPS might be sufficient
			STRETCH GOALS: 
				Detect “canopy” height and inform drone.
recharge drone
collect/deliver payload (anything sort of stand in would work)
	Server:
		Plot courses
		Accept connections from pads and possibly drone.


	
Questions: 
how much do cellular chips cost?
- It looks like the way to go would be a USB dongle plugged into the PI. It would cost between 50 and 100 dollars.
Can we use multiple battery packs in parallel?
Yes but it requires some work to prevent the batteries from fighting. Most people recommend manually balancing them and using them as one battery from then on.
It does look like some people just wire them up though.

Misc. Ideas
Have lights on the drone as indicators for what it is doing
Visual indicators for landing, taking off, whether it has comm with the pad, whether it can visually see the pad. Lights for low battery and lights for mobility errors (the drone is not flying as it expects such as might happen in high wind.)
If we have obstacle detection then a light for “I see an obstacle” possibly a light in the direction of the obstical.
RGB lights would give three channels for info, a ring of them gives directional information, flashing lights gives another dimension. 
Obstacle detection could be done with a ring of ultrasonics (even better if paired with infrared sensors). An octocopter can easily have eight range sensors at the end of each arm.
I think the idea of having a large helium balloon attached to the drone sounds silly, but honestly for our purposes, I wonder what the issue would be. It would make the drone slower, but more stable and n having a longer battery life. Perfect for our purposes.
Looks like this has been done, sort of

Plan:
	We need to split this into tasks + subtasks
		A.1: F Drone physical design
		B.1: RE Drone calibration
		B.2: RE Drone automated flight
		B.3: RE Drone fly by GPS
		B.4. RE Drone adapt to final drone hardware
A.2: F drone electrical design/component selection
Electrical component mounting
A.3: F drone assembly
C.1: PAD prototype
	C.2: PAD design 
	C.3: PAD assembly
	D.1: Vision detection development
	D.2: Vision integration
	E.1: Server Implementation
	E.2: Control Interface
	E.3: Drone/pad communication
	We also should list out the cross-dependencies/overlaps.
		1: the flight board: Relevant to A.2+, B.4, 
		2. The control board: Relevant to A.2+, D, and B.2+
This is may be a raspberry pi zero, but that might not have enough power for vision. Whatever it is it needs to be light, low power, run linux, and have enough processing to do the image detection real time.
DECIDED BY: whoever does vision processing will need to figure out what board is sufficient. 
We want to try out:
	Beaglebone (benefits from being able to use xenomai)
	Raspberry pi: not sure what advantage this has over beaglebone
	Raspberry pi zero: tiny and light, ultra low power
	The Chip: similar to raspberry pi
	WHAT ELSE: there may be other options.
- it would be good to use a board made for industrial applications. The Pi zero is not, while the beaglebone is.
		3. Drone communication:
We can either have the drones talk to the pads over wifi or the server directly over cellular.








Alex: is going to look into other University’s report on same project

Tommy: Visual tracking, (main way to find pad/ no gps ???)

Ryan: pad to drone communications/ pad stuff (how to make it, components for comm, easy image to recognize)

Erich: flight controller software
Barometer sensor is not precise enough for altitude control
Spf3 doesn’t seem to have a accel based “stay in place” or stay at height mode
We might want downward mounted range finders (lidar?)
Drone flys, but hard to control.


Shaqqqq: physical design of drone

Controllers:
	[$132]: Beagleboard blue: $82
  external GPS/cellular: [adafruit $50]
	[$265]: Raspberry PI: $35
 NAVIO2: $168
 Cellular:  [$50?] 
 GPS antenna: $12

GPS: 
GPS+Cellular [$50] https://www.adafruit.com/product/2542 

Motors:
torque vs horsepower?
Looks to me like we want high diameter props
Choose motors after weight, size, and prop size are known
Pick ESCs to be just powerful enough for motors
At least 6 arms
Not a lot of people seem to do octocopters, I can't figure out why.

PAD:
	I’m not ruling out charging entirely, but let’s call that a very stretch goal.
	However we could design the pad such that it is easy to show how the pad charging the drone would work if added.
	? LED backlight for night time?
	? homing beacon ?

Drone:
	Perhaps a hexacopter with 12” props
	As an example these would be more than powerful enough. They pull 2kg each and cost $80 for four. https://www.banggood.com/4X-Racerstar-Racing-Edition-4114-BR4114-400KV-4-8S-Brushless-Motor-For-600-650-700-800-RC-Frame-Kit-p-1124378.html?cur_warehouse=CN
	Lets budget $160 for motors. Another $50-$100 for battery
	If the total drone cost is kept under $400 we could easily build two and do a sort of leapfrog demonstration. This is do-able if we use the BBBlue control stack and keep the cost of motors under $100 and the frame + bat + etc come in under $250. That would leave $450 for the pads and anything else? The sensible thing to do is try to keep the drone cheap, and towards the end build a second one if time and funds allow and we think it would enhance the presentation.

	Range finders. Laser: as cheap as $30, decent ones are $100+
We would only use laser for a downward mounted height sensor. Unnecessary if the barometer + accel is good enough
Ultrasonics: dirt cheap generally, but we need to order them if we want to use them since generally they come from china.
Inferred: bad in sunlight, just all around inappropriate for our purposes

Tools:
	Something to hold the copter down during testing
	PWM generator (i think there is one in the lab) for testing esc/motor


